import * as plugins from './smartreport.plugins';
import { ReportSection } from './smartreport.classes.reportsection';

export class Smartreport {
  public reportSections: ReportSection[] = [];

  /**
   *  constructor
   */
  constructor() {}

  public addReportSection(reportSectionArg: ReportSection) {
    this.reportSections.push(reportSectionArg);
  }
}
