import * as plugins from './smartreport.plugins';
import { ReportItem } from './smartreport.classes.reportitem';

export class ReportSection {
  public name: string;
  public reportItems: ReportItem[] = [];

  constructor(nameArg: string) {
    this.name = nameArg;
  }

  public addReportItem(reportItem: ReportItem) {
    this.reportItems.push(reportItem);
  }
}
